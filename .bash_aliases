alias ve='source .virtualenv/bin/activate || source ~/.virtualenv/bin/activate'
alias tra='export PYTHONPATH='$HOME/projects/trex-analytics/src'; cd $HOME/projects/trex-analytics && ve'
alias rap='export PYTHONPATH='$HOME/projects/raptor/src'; cd $HOME/projects/raptor && ve'
alias tdw="cd $HOME/projects/tdw && ve"

# alias utest="SELENIUM_DRIVER='firefox' src/manage.py test --settings=analytics.settings.test"
alias udtest="~/projects/trex-analytics/testdb.sh -a && src/manage.py test --keepdb --settings=analytics.settings.test"
alias utest="src/manage.py test --keepdb --settings=analytics.settings.test"
alias ntest="SELENIUM_DRIVER='firefox' src/manage.py test --settings=analytics.settings.test_nom"
alias mc="curl -H 'Authorization: Token 486ff9eba932bdcddb1232eac4f870603446db64'"
alias weather="curl -sk https://wttr.in/nyc"
alias timer="termdown -f standard"
alias tmux="env TERM=tmux-256color tmux"
alias mutt="TERM=tmux-256color mutt"
alias s='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/s.py'
alias ts='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/ts.py'
# alias release_status='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/release_status.py'
alias release_status='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/.virtualenv/bin/python $HOME/projects/kpis/utils/release_status.py'
alias copy_release_status='$HOME/settings/utils/copy_release'
alias violations='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/violations.sh'
alias violations_stats='export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/violations_stats.sh'

alias less='less -R'
alias showcal='~/settings/utils/showcal.py'
alias weekly='~/settings/utils/showcal.py -d 7 arad'
alias oo='vim "$(find ~/db/tv/* -type f | fzf)" -c ":cd ~/db/tv" -c "colorscheme Tomorrow" -c "Goyo" -c "set scrolloff=999"'
alias nb='vim -c "colorscheme Tomorrow" -c "Goyo" -c "set scrolloff=999" -c "set background=light"'
alias o='vim "$(find ~/db/tv/* -type f | fzf)" -c ":cd ~/db/tv"'
alias tv='cd ~/db/tv && vim'
alias stv='~/settings/utils/stv.sh'
# alias tv="terminal_velocity --extension=md ~/db/tv"
alias main='tmuxp load 0'
alias tw='timew'

alias vlc='vlc -I ncurses --no-color'


## All TaskWarrior shortcuts
alias tadd="task add sch:tod"
alias tu="task add due:tod sch:tod"
alias t="task"
alias tt="vim -c ':TW +READY -SOMEDAY'"
alias n="task ready limit:5"
alias n1="task limit:5"
alias tod="task due:tod"
alias tom="task +TREX due:tom"
alias ptod="task -TREX due:tod"
alias ptom="task -TREX due:tom"
alias old="task scheduled.before:tod"
alias hs="task project:house"
alias overdue="task due.before:tod"
alias next="task due:tod limit:5"
alias week="task sch.after:socw sch.before:sow"
alias postpone="task status:pending due:tod mod due:tom"
alias keepup="task status:pending due:yest mod due:tod"
alias tpm='task mod sched:$(task calc eom)'
alias tpd="task mod sched:tom"
alias tpw="task mod sched:mon"
alias donetoday='~/settings/utils/donetoday'
alias cmpr='~/settings/utils/cmpbranches'

alias bat="bat --theme=zenburn"
# alias vim="nvim"

alias ag='ag --path-to-ignore ~/.ignore'

alias gcal='gcalcli --calendar "Hannah Senesh Calendar"#yellow --calendar "Arad Shaiber"#blue --calendar "Hela and Arad"#green --calendar "Holidays in United States"#red --calendar "Hela Lahar" '
alias open='xdg-open'
alias msgtelegram='~/.virtualenv/bin/python ~/settings/utils/msgtelegram.py'

alias dl='vim ~/db/tv/dailys/$(date --iso-8601)-daily.md'

