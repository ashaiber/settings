In Cygwin, run the following to get 256-colors screen:

  TERM=screen-256color screen

and then in screen, run:

  export TERM='xterm-256color'


## Adding a new plugin for vim:

Under settings/, run:

    git submodule add git://github.com/tpope/vim-fugitive.git .vim/bundle/vim-fugitive

Then Commit:

    git commit -m 'Added vim-fugitive'
