#!/usr/bin/env python3
"""
Simple utility for showing team calendars
"""
import os
import datetime
import click


CALENDARS = [
    "Arad Shaiber",
    "Benjamin Cohen",
    "Birthdays for T-REX Group, Inc.",
    "Contacts",
    "Hela & Arad's Calendar",
    "Hela and Arad",
    "Holidays in Israel",
    "Scott Miller",
    "Who's Out",
    "akiva.elkayam@trexgroup.com",
    "arad.shaiber@trexgroup.com",
    "ben.armenti@trexgroup.com",
    "ben.ross@trexgroup.com",
    "eitan.yarimi@trexgroup.com",
    "elad.yaron@trexgroup.com",
    "evnt@trexgroup.com",
    "gonen.shemesh@trexgroup.com",
    "hagit.muskatel@trexgroup.com",
    "michael.weiss@trexgroup.com",
    "nandita.ray@trexgroup.com",
    "natalie.shalev@trexgroup.com",
    "nira.roth@trexgroup.com",
    "noemie.ghariani@trexgroup.com",
    "pedro.nissani@trexgroup.com",
    "rami.weiss@trexgroup.com",
    "sarah.holt@trexgroup.com",
    "tricia.sancristobal@trexgroup.com",
    "yuri.sanspeur@trexgroup.com",
]


@click.command()
@click.argument("name")
@click.option("-d", "--days", help="How many days ahead")
def main(name, days):

    if not days:
        days = 1
    else:
        days = int(days)
    # utility = ('/home/arad/.virtualenv/bin/gcalcli --calendar "{}"' +
    today = datetime.date.today()
    delta = datetime.timedelta(days=days)
    tomorrow = today + delta
    today_iso = today.isoformat()
    tomorrow_iso = tomorrow.isoformat()

    home = os.environ["HOME"]
    utility = f"{home}/.local/bin/gcalcli"
    arguments = '--calendar "{}" agenda {} {} --military --nocache'

    matches = [a for a in CALENDARS if a.lower().find(name.lower()) >= 0]
    for match in matches:
        print("** {} **".format(match))
        args = arguments.format(match, today_iso, tomorrow_iso)
        os.system(utility + " " + args)


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    main()
