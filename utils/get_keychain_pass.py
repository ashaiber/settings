#!/usr/bin/python
import re
import subprocess
import argparse


def get_keychain_pass(account=None, server=None):
    params = {
        'security': '/usr/bin/security',
        'command': 'find-internet-password',
        'account': account,
        'server': server,
        'keychain': '/Users/arad/Library/Keychains/login.keychain',
    }
    command = "sudo -u arad %(security)s -v %(command)s -g -a %(account)s -s %(server)s %(keychain)s" % params
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    outtext = [l for l in output.splitlines()
               if l.startswith('password: ')][0]

    return re.match(r'password: "(.*)"', outtext).group(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--service', type=str, required=True,
                        help='Keyring domain')
    parser.add_argument('-u', '--user', type=str, required=True,
                        help='User to query password for')

    args = parser.parse_args()

    pwd = get_keychain_pass(args.user, args.service)

    print(pwd)
