#!/bin/bash

platform='unknown'
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
    platform='linux'
elif [[ "$unamestr" == 'FreeBSD' ]]; then
    platform='bsd'
elif [[ "$unamestr" == 'Darwin' ]]; then
    platform='mac'
fi

if [[ $platform == 'linux' || $platform == 'unknown' ]]; then
    while true; do
        clear; gcalcli --calendar "arad.shaiber@trexgroup.com" --calendar "Who's Out" --calendar "Arad Shaiber" --calendar 'Hela and Arad' agenda `date +'%Y-%m-%d'` `date --date="2 days" +'%Y-%m-%d'` --military
        sleep 300
    done

elif [[ $platform == 'mac' ]]; then
    while true; do
        # clear; gcalcli --calendar "arad.shaiber@trexgroup.com" --calendar "Who's Out" agenda `date +'%Y-%m-%d'` `date -v+2d +'%Y-%m-%d'` --military
        clear; gcalcli --calendar "arad.shaiber@trexgroup.com"#white --calendar "Who's Out"#magenta --calendar "Arad Shaiber"#green --calendar 'Hela and Arad'#green agenda `date +'%Y-%m-%d'` `date -v+2d +'%Y-%m-%d'` --military
        sleep 300
    done
fi
