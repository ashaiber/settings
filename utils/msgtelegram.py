"""
This utility can be used to send a simple message to my telegram account

The API key should be placed under ~/.config/telegram/key
"""
import os

import click
import telegram

HOME = os.getenv("HOME")
CHAT_ID = "642263863"


@click.command()
@click.argument("message")
def main(message):
    with open(f"{HOME}/.config/telegram/key", "r", newline="") as key_file:
        key = key_file.read()
        bot = telegram.Bot(key.strip())
        bot.send_message(chat_id=CHAT_ID, text=message)
        # send_message(chat_id=chat_id, text="I'm sorry Dave I'm afraid I can't do that.")


if __name__ == "__main__":
    main()
