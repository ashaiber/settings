#!/usr/bin/env python
from datetime import datetime

RATE = 22
FMT = "%H:%M"
HOURS = [
    ['1:00', '5:45'],
    ['1:00', '6:00'],
    ['1:00', '3:00']
]


def diff(s1, s2):
    import pdb; pdb.set_trace()
    st1 = datetime.strptime(s1, FMT)
    st2 = datetime.strptime(s2, FMT)
    delta = (st2 - st1).seconds / 60
    print(delta)
    return delta

total_mins = sum(diff(x[0], x[1]) for x in HOURS)

print("Hours: ", total_mins / 60.0)
print("Total: ", RATE * total_mins / 60.0)
