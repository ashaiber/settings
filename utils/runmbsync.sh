#!/bin/bash

# Kill hung instances of mbsync
killall mbsync &>/dev/null
mbsync -a -q
