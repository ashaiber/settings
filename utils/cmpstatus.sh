DAY_TO=`date "+%Y-%m-%d"`
DAY_FROM='2019-03-21'

if [ -n "$1" ]; then # First parameter is from-date
    DAY_FROM=$1
fi

if [ -n "$2" ]; then # First parameter is to-date
    DAY_TO=$2
fi

echo Comparing $DAY_FROM To $DAY_TO

export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/release_status.py -d $DAY_TO -p ~/db/ 
export PYTHONPATH="$HOME/projects/kpis/"; $HOME/projects/kpis/utils/release_status.py -d $DAY_FROM -p ~/db/ 

cut -d , -f 1,3,4,7 ~/db/$DAY_TO-daily-progress.csv > /tmp/$DAY_TO.csv 
cut -d , -f 1,3,4,7 ~/db/$DAY_FROM-daily-progress.csv > /tmp/$DAY_FROM.csv 

colordiff /tmp/$DAY_FROM.csv /tmp/$DAY_TO.csv
