#!/bin/bash

while true; do
    cd ~/projects/trex-analytics
    git fetch > /dev/null
    TRA=$(git rgoingon | grep -v 'Merge' | head -30 | sed "s/$/ {} /")
    # TRA=$(git goingon | grep -v 'Merge' | head -15 | sed "s/$/ /")
    cd ~/projects/titan
    git fetch > /dev/null
    TITAN=$(git rgoingon | grep -v 'Merge' | head -30 | sed "s/$/ {} /")
    # TITAN=$(git goingon | grep -v 'Merge' | head -15 | sed "s/^/ /")
    clear; 
    echo -e "$TRA","\n$TITAN" | sort | tail -25 | perl -n -e'/\(([0-9\-:\+T]+)\) \(([0-9a-zA-Z ]*)\) (.*) \(([a-zA-Z]*).*\) \{(.*)\}/ && print "\033[1;32m$5 \033[1;33m$2 \033[0m$3 \033[0;32m[$4]\033[0m\n"' | uniq

    sleep 600
done
