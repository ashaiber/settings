#!/bin/bash

cd $HOME/db/tv

msgtel="$HOME/.virtualenv/bin/python $HOME/settings/utils/msgtelegram.py"

if ! git pull > /dev/null ; then
    eval $msgtel "Could not run git pull on TV"
fi

git add . > /dev/null 
git commit -m "Updates from arad-dell" --quiet > /dev/null
git push --quiet > /dev/null
