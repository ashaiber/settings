#!/bin/bash

RED='\033[0;31m'
BRED='\033[1;31m'
GREEN='\033[0;32m'
BGREEN='\033[1;32m'
IGREEN='\033[3;32m'
YELLOW='\033[0;33m'
BYELLOW='\033[1;33m'
UWHITE='\033[4;37m'
WHITE='\033[0;37m'
NC='\033[0m' # No Color

while true; do
    OUTPUT1=`psql -U postgres -h localhost -p 9501 nano -t -c 'select array_to_json(array_agg(row_to_json(t))) from (SELECT date_part('"'"'year'"'"', date_time::date) as year, date_part('"'"'week'"'"', date_time::date) AS weekly, COUNT(name) FROM events WHERE date_time>'"'"'2017-01-01'"'"' GROUP BY year, weekly ORDER BY year, weekly) t;' | jq -r '.[] | "\(.count)"' | spark`

    # clear

    echo -e "${UWHITE}Weekly Events${NC}"
    echo -e "$OUTPUT1"
    # echo ""
    # echo -e "${UWHITE}Error message over the last 24 hours${NC}"
    # echo -e "${RED}"
    # echo -e "$OUTPUT2"
    # echo -e "${NC}"

    sleep 120
done
