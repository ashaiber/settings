#!/bin/bash

RED='\033[0;31m'
BRED='\033[1;31m'
GREEN='\033[0;32m'
BGREEN='\033[1;32m'
IGREEN='\033[3;32m'
YELLOW='\033[0;33m'
BYELLOW='\033[1;33m'
UWHITE='\033[4;37m'
WHITE='\033[0;37m'
DARKGRAY='\e[38;5;239m'
BRIGHTBLACK='\033[0;90m'
NC='\033[0m' # No Color




COLOR=${GREEN}

# echo "$OUTPUT"
# echo "$GOALCOUNT"

while true; do
    OUTPUT=`task +READY due.before:today export | jq -r '.[] | "[\(.id)]\t\(.description)"'`
    GOALCOUNT=`echo "${OUTPUT}" | wc -l`

    clear

    echo -e "${UWHITE}Overdue tasks${NC}"
    echo ""

    if [ -z "$OUTPUT" ]
    then
        echo -e "${IGREEN}All done${NC}"
    elif [ $GOALCOUNT == 1 ]
    then
        COLOR=${BGREEN}
        echo -e "${COLOR}${OUTPUT}${NC}"
    elif [ $GOALCOUNT == 2 ]
    then
        COLOR=${BYELLOW}
        echo -e "${COLOR}${OUTPUT}${NC}"
    elif [ $GOALCOUNT > 2 ]
    then
        COLOR=${BRED}
        echo -e "${COLOR}${OUTPUT}${NC}"
    fi

    echo ""
    echo -e "${UWHITE}Top 5 tasks${NC}"
    echo ""
    OUTPUT=`task +READY export 2> /dev/null | jq -r 'sort_by(-.urgency) | .[] | "[\(.id)]\t\(.description)"' | head -5`

    if [ -z "$OUTPUT" ]
    then
        echo -e "${IGREEN}All done${NC}"
    else
        COLOR=${GREEN}
        echo -e "${COLOR}${OUTPUT}${NC}"
    fi

    echo ""
    echo -e "${UWHITE}Accomplished today${NC}"
    echo ""
    OUTPUT=`task status:completed end:today export 2> /dev/null | jq -r 'sort_by(.end) | .[] | " \t\(.description)"' `

    if [ -z "$OUTPUT" ]
    then
        echo -e "${IGREEN}Nothing yet :(${NC}"
    else
        COLOR=${GREEN}
        echo -e "${BRIGHTBLACK}${OUTPUT}${NC}"
    fi

    sleep 60
done
