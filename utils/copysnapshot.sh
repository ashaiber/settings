#!/bin/bash

TODAY=`date "+%Y-%m-%d"`

SNAPSHOT_ID=`aws rds describe-db-snapshots --query "DBSnapshots[?SnapshotCreateTime>='$TODAY'].DBSnapshotIdentifier" --snapshot-type automated --db-instance-identifier production-analytics-rds-db-4`

echo $SNAPSHOT_ID
