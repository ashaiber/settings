#!/bin/bash

# TODO:
# For yesterday, I want to see total hours for each user.
# For today I want to see the same as before
# For errors, I want to see the same as before

RED='\033[0;31m'
BRED='\033[1;31m'
GREEN='\033[0;32m'
BGREEN='\033[1;32m'
IGREEN='\033[3;32m'
YELLOW='\033[0;33m'
BYELLOW='\033[1;33m'
BLUE='\033[0;34m'
IBLUE='\033[3;34m'
UWHITE='\033[4;37m'
WHITE='\033[0;37m'
NC='\033[0m' # No Color


# SERVER='localhost'
SERVER='192.168.128.152'

while true; do
    OUTPUT0=`psql -U postgres -h $SERVER -p 9501 nano -t -c 'select array_to_json(array_agg(row_to_json(t))) from (SELECT first_name, last_name, organization, (date_time::timestamptz AT TIME ZONE '"'"'EST'"'"')::date AS only_date, SUM(time_delay) AS total_time FROM (SELECT u.first_name, u.last_name, u.organization, e.user_uuid, e.date_time::timestamptz AT TIME ZONE '"'"'EST'"'"' AS date_time, (e.date_time-lag(e.date_time) OVER (PARTITION BY e.user_uuid ORDER BY e.date_time)) AS time_delay FROM events AS e INNER JOIN users AS u ON e.user_uuid=u.uuid WHERE (e.date_time::timestamptz AT TIME ZONE '"'"'EST'"'"')::date >= (NOW()::timestamptz AT TIME ZONE '"'"'EST'"'"')::date - '"'"'1 day'"'"'::interval AND (e.date_time::timestamptz AT TIME ZONE '"'"'EST'"'"')::date < (NOW()::timestamptz AT TIME ZONE '"'"'EST'"'"')::date) t WHERE time_delay < '"'"'20 minutes'"'"' AND user_uuid<>'"'"''"'"' GROUP BY first_name,last_name,organization,only_date ORDER BY only_date) t;' | jq -r '.[] | "\(.first_name) \(.last_name)\t(\(.organization[0:13]))\t\(.total_time)"' | column -t -s $'\t'`

    OUTPUT1=`psql -U postgres -h $SERVER -p 9501 nano -t -c 'select array_to_json(array_agg(row_to_json(t))) from (select u.first_name,u.last_name,u.organization,count(*),e.name from events as e inner join users as u on u.uuid=e.user_uuid where (e.date_time::timestamptz AT TIME ZONE '"'"'EST'"'"')::date >= (NOW()::timestamptz AT TIME ZONE '"'"'EST'"'"')::date GROUP BY u.first_name,u.last_name,e.name,u.organization ORDER BY first_name,count DESC) t;' | jq -r '.[] | "\(.first_name) \(.last_name)\t(\(.organization[0:13]))\t\(.count)x\t\(.name)"' | column -t -s $'\t'`

    OUTPUT2=`psql -U postgres -h $SERVER -p 9501 nano -t -c 'select array_to_json(array_agg(row_to_json(t))) from (select count(*),e.properties->'"'"'message'"'"' AS message, u.first_name, u.last_name from events as e inner join users as u on u.uuid=e.user_uuid where e.name = '"'"'message shown'"'"' and (e.date_time::timestamptz AT TIME ZONE '"'"'EST'"'"')::date >= (NOW()::timestamptz AT TIME ZONE '"'"'EST'"'"')::date - '"'"'1 day'"'"'::INTERVAL group by u.first_name,u.last_name,message) t;' | jq -r '.[] | "\(.first_name) \(.last_name)\t\(.count)x\t\(.message[0:80])…"' | column -t -s $'\t'`

    clear

    date
    echo ""

    echo -e "${UWHITE}Yesterday's Summary${NC}"
    echo -e "${BLUE}"
    echo -e "$OUTPUT0"
    echo ""
    echo -e "${UWHITE}Today's Events${NC}"
    if [ -z "$OUTPUT1" ]
    then
        echo ""
        echo -e "${IBLUE}No events${NC}"
    else
        echo -e "${GREEN}"
        echo -e "$OUTPUT1"
        echo -e "${NC}"
    fi
    echo ""
    echo -e "${UWHITE}Error message over the last 24 hours${NC}"
    if [ -z "$OUTPUT2" ]
    then
        echo ""
        echo -e "${IBLUE}No messages${NC}"
    else
        echo -e "${RED}"
        echo -e "$OUTPUT2"
        echo -e "${NC}"
    fi

    sleep 120
done
