#!/bin/sh -e


# This script can be used to scan from the scansnap s1100 in the Dell-Ubuntu

nr=1


echo "" > /tmp/scan_file_list.txt
output=/home/arad/nextcloud/INBOX/scan-`date -Iseconds | sed -e "s/://g"`
tmpfolder=/tmp/scan-`date -Iseconds | sed -e "s/://g"`
mkdir $tmpfolder

while true ; do
        file=$tmpfolder/`printf "%04d" $nr`.tiff
        echo "scanning page $nr -> $file"
        scanimage --progress --resolution 300 --mode gray --format tiff > $file
        echo "$file" >> /tmp/scan_file_list.txt
        # eog $file
        echo "enter = one more page, anything else to convert to $1.pdf"
        read wait_for_key
        if [ ! -z "$wait_for_key" ] ; then
                # convert $1/*.jpg $1.pdf
                # mupdf $1.pdf
                # ls -al $1.pdf
                tesseract /tmp/scan_file_list.txt $output -l eng pdf
                exit 0
        fi
        nr=`expr $nr + 1`
        echo "Saved PDF to $output.pdf"
done
