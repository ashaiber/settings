source ~/.vimrc_common
""" General settings
"Settings for Autoload
" filetype off
"" call pathogen#infect()

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'airblade/vim-gitgutter'
" Plugin 'scrooloose/syntastic'
" Asynchronous Lint Engine
Plugin 'w0rp/ale'
Plugin 'pangloss/vim-javascript'
"vim plug-in which provides support for expanding abbreviations similar to "emmet 
" Plugin 'mattn/emmet-vim'
" Plugin 'rking/ag.vim'
" Plugin 'mrtazz/simplenote.vim'
Plugin 'tpope/vim-unimpaired'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-surround'
" VueJS plugin
Plugin 'posva/vim-vue'
"
" Programming language auto-completion plugin and its dependencies
" jedi-vim is a VIM binding to the autocompletion library Jedi.
Plugin 'davidhalter/jedi-vim'
" Plugin 'altercation/vim-colors-solarized'
Plugin 'dhruvasagar/vim-table-mode'
Plugin 'scrooloose/nerdtree'
" Start screen for VIM
Plugin 'mhinz/vim-startify'
" Plugin for Tmux-statusline
Plugin 'edkolev/tmuxline.vim'
" More icons
Plugin 'ryanoasis/vim-devicons'
" FZF Vim plugin
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'

" Markdown
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'vim-pandoc/vim-pandoc-syntax' 
" NOTE - this is what I used up to 2017-09, but annoyed with list treatment. Plugin 'gabrielelana/vim-markdown'

" Haskell
Plugin 'eagletmt/ghcmod-vim'
Plugin 'eagletmt/neco-ghc'
Plugin 'tomtom/tlib_vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'garbas/vim-snipmate'

" CSV file plugin
Plugin 'chrisbra/csv.vim'

" GOYO - for writing focused mode
Plugin 'junegunn/goyo.vim'

" Commenting Plugin
" Plugin 'scrooloose/nerdcommenter'
" Plugin 'godlygeek/tabular'
"vim plugin which allows you to use <Tab> for all your insert completion needs
Plugin 'ervandew/supertab'
Plugin 'Shougo/vimproc.vim'

" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}
"

" Jedi-vim settings
" let g:jedi#completions_enabled = 0
let g:jedi#usages_command = "<leader>u"
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_command = "<leader>d"
" let g:jedi#force_py_version = 3

" ale (linter) setting
let g:ale_lint_on_text_changed = 'never'
let g:ale_fixers = {'python': ['black',]}

" This controls how often Vim updates the buffer. For example, with gitgutter,
" this will determine how often the gutter is re-evaluated
set updatetime=200

" All of your Plugins must be added before the following line
call vundle#end()            " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" vim-table-mode plugin settings
let g:table_mode_corner="|"


"Use F6 to toggle to Tomorrow (Light)
map <F6> :colo Tomorrow<CR>
map <F5> :colo Tomorrow-Night<CR>
map <F2> :set norl<CR>
map <F3> :set rl<CR>

" Set default statusline (show with lastline=2)
" set statusline=%F%m%r%h%w\ [%{&ff}][%Y]\ [%l/%L][%p%%]

" Make command and history list longer
set history=1000

""Settings for Pydiction (auto-completion for Python)
let g:pydiction_location = '~/.vim/ftplugin/pydiction/complete-dict'

"" CtrlP plugin
set runtimepath^=~/.vim/bundle/ctrlp.vim
" nnoremap <leader>p :CtrlPBuffer<CR>

" set wildignore+=*/bin/*,*/obj/*,*/build/*
set wildignore+=*.so,*.swp,*.zip,*.pyc
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules|migrations)$',
  \ 'file': '\v\.(exe|so|dll|cache|pdb|suo)$',
  \ }

" Tabs

"Make CTags look up for a file in parent folders
set tags=tags;/

"" Syntastic

" map <leader>a :SyntasticToggleMode<CR>

" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

""" Custom Key Mappings
"Set a shortcut for going into 'paste' mode
set pastetoggle=<c-e>

"Shortcuts for Tabular
nmap <leader>t :Tabularize /\|<CR>
vmap <leader>t :Tabularize /\|<CR>

"Shortcut for Marked (Only on OS-X)
:nnoremap <leader>o :silent !open -a Marked.app '%:p'<cr>

"Shortcuts for using Fuzzy
" nnoremap ,b :FufBuffer<cr>

"Shortcuts (and configuration) for taglist
" binds ,l to taglist
" nnoremap ,l :TlistToggle<CR>
" let Tlist_Show_Menu=1
" let Tlist_GainFocus_On_ToggleOpen=1
" let Tlist_Close_On_Select=1
" let Tlist_Compact_Format=1


"Shortcut for themes
nnoremap <leader>n :colorscheme Tomorrow-Night<CR>
nnoremap <leader>m :colorscheme Tomorrow<CR>
nnoremap <leader>b :colorscheme Tomorrow-Night-Trans<CR>
nnoremap <leader>v :colorscheme Tomorrow-Trans<CR>

"Folding for markdown files
function MarkdownLevel() 
    let h = matchstr(getline(v:lnum), '^###\+') 
    if empty(h) 
        return "=" 
    else 
        return ">" . len(h) 
    endif 
endfunction

""" Language specific
"" Python
"Currently Disabled: Add all Python paths so that gf can be used to browse Python libraries
"python << EOF
"import os
"import sys
"import vim
"for p in sys.path:
    "if os.path.isdir(p):
        "vim.command(r"set path+=%s" % (p.replace(" ", r"\ ")))
"EOF
"

"Make Vim enforce PEP8 for Python files
" The following comment is disabled since it has problems with comments:
"autocmd BufNewFile,BufRead *.py setlocal smartindent

"" C#
autocmd BufNewFile,BufRead *.cs setlocal tabstop=4
autocmd BufNewFile,BufRead *.cs setlocal softtabstop=4
autocmd BufNewFile,BufRead *.cs setlocal shiftwidth=4
autocmd BufNewFile,BufRead *.cs setlocal textwidth=100
autocmd BufNewFile,BufRead *.cs setlocal smarttab
autocmd BufNewFile,BufRead *.cs setlocal expandtab
autocmd BufNewFile,BufRead *.cs setlocal smartindent

"" Javascript + Vue
autocmd BufNewFile,BufRead *.vue setlocal tabstop=2
autocmd BufNewFile,BufRead *.vue setlocal softtabstop=2
autocmd BufNewFile,BufRead *.vue setlocal shiftwidth=2
autocmd BufNewFile,BufRead *.js setlocal tabstop=2
autocmd BufNewFile,BufRead *.js setlocal softtabstop=2
autocmd BufNewFile,BufRead *.js setlocal shiftwidth=2

"" HTML
autocmd BufNewFile,BufRead *.html setlocal tabstop=2
autocmd BufNewFile,BufRead *.html setlocal softtabstop=2
autocmd BufNewFile,BufRead *.html setlocal shiftwidth=2
autocmd BufNewFile,BufRead *.html setlocal textwidth=200
autocmd BufNewFile,BufRead *.html setlocal smarttab
autocmd BufNewFile,BufRead *.html setlocal expandtab
autocmd BufNewFile,BufRead *.html setlocal smartindent
autocmd BufNewFile,BufRead *.hbs setlocal tabstop=2
autocmd BufNewFile,BufRead *.hbs setlocal softtabstop=2
autocmd BufNewFile,BufRead *.hbs setlocal shiftwidth=2
autocmd BufNewFile,BufRead *.hbs setlocal textwidth=200
autocmd BufNewFile,BufRead *.hbs setlocal smarttab
autocmd BufNewFile,BufRead *.hbs setlocal expandtab
autocmd BufNewFile,BufRead *.hbs setlocal smartindent

"" Mail (Mutt)
autocmd BufNewFile,BufRead mutt-Arad* set filetype=mail

"" Yaml
autocmd BufNewFile,BufRead *.yml setlocal tabstop=2
autocmd BufNewFile,BufRead *.yml setlocal softtabstop=2
autocmd BufNewFile,BufRead *.yml setlocal shiftwidth=2

"" Haskell
autocmd BufNewFile,BufRead *.hs setlocal textwidth=100
autocmd BufNewFile,BufRead *.hs setlocal smarttab
autocmd BufNewFile,BufRead *.hs setlocal expandtab
autocmd BufNewFile,BufRead *.hs setlocal tabstop=2
autocmd BufNewFile,BufRead *.hs setlocal softtabstop=2
autocmd BufNewFile,BufRead *.hs setlocal shiftwidth=2

"" XAML
autocmd BufNewFile,BufRead *.xaml setf xml
autocmd BufNewFile,BufRead *.hbs setf html

"" log files (i.e. log4net, etc.)
au! BufRead,BufNewFile *.log,*.log.* setf log
augroup END

