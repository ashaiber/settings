" Vim syntax file
" file type: log files
" Quit when a (custom) syntax file was already loaded
if exists("b:current_syntax")
finish
endif
syn match fatal ".* FATAL .*"
syn match fatal ".* FATAL: .*"
syn match fatal ".* Fatal .*"
syn match fatal "^FATAL: .*"
syn match error ".* ERROR .*"
syn match error ".* ERROR: .*"
syn match error ".* Error .*"
syn match error "^ERROR: .*"
syn match warn ".* Warn .*"
syn match warn ".* WARN .*"
syn match warn ".* WARN: .*"
syn match warn ".* WARNING .*"
syn match warn ".* WARNING: .*"
syn match warn "^WARN: .*"
syn match info ".* INFO .*"
syn match info ".* INFO: .*"
syn match info ".* Info .*"
syn match info "^INFO: .*"
syn match debug ".* DEBUG .*"
syn match debug ".* DEBUG: .*"
syn match debug ".* Debug .*"
syn match debug "^DEBUG: .*"

" Highlight colors for log levels.
hi fatal cterm=bold ctermfg=Red ctermbg=NONE
hi error cterm=bold ctermfg=Red ctermbg=NONE
hi warn cterm=bold ctermfg=Magenta ctermbg=NONE
hi info ctermfg=Gray ctermbg=NONE
hi debug cterm=italic ctermfg=Gray ctermbg=NONE

let b:current_syntax = "log"

" vim: ts=2 sw=2
