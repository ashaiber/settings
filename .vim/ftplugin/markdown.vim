" (Arad) settings for mail messages in mutt
" Set line width to 72 characters
setl tw=80
" Set 'w' to support flowed format, which adds an empty space to soft line
" breaks
setl fo=aw
