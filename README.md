My Linux/OS-X/Cygwin Settings files
-----------------------------------

After cloning, run:

    git submodule init
    git submodule update

or:

    git submodule update --init --recursive

    ln -s settings/.bashrc 
    ln -s settings/.bash_profile
    ln -s settings/.vim 
    ln -s settings/.vimrc 
    ln -s settings/.config 
    ln -s settings/.inputrc 
    ln -s settings/.pylintrc 
    ln -s settings/.gitconfig 
    ln -s settings/.tmux.conf
